# Rust Actix Web Service

This is a simple web service written in Rust using the Actix Web framework. It is a simple example of how to create a web service in Rust.

## Requirements

- Evaluate different virtualization abstractions.
- Build solutions with containers.
- Build solutions with virtual machines.

## Dependencies
- actix-web

## Steps to run the project

- Use Cargo to build the project
```bash
cargo init
```

- Add components to main.rs and lib.rs

- Run the project
```bash
cargo run
```

- Open a browser and navigate to http://localhost:8080

- Build the project
```bash
cargo build --release
```

## Docker commands
Build the Docker image
```bash
docker build -t actix-webapp .
```

Run the Docker container
```bash
docker run -p 8080:8080 actix-webapp
```
![](screenshots/docker_usage.png)

## Test the web service
Go to http://localhost:8080 and you should see the following message:
![](screenshots/webserver1.png)

Test the web service with URL parameters
![](screenshots/webserver2.png)
