# Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY  . .
RUN cargo build --release

# Production stage
FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/mini_project4_actix_webapp /app/

# use non-root user
USER nonroot:nonroot

# Set up App directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

# Expose the port
EXPOSE 8080

# Run the web service on container startup.
ENTRYPOINT [ "/app/mini_project4_actix_webapp" ]
